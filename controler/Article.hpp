#ifndef BLOG_CONTROLER_ARTICLE_H_
#define BLOG_CONTROLER_ARTICLE_H_

#include <vector>
#include "../model/Article.hpp"
#include "../model/Sqlite.hpp"

namespace blog {

namespace view  {class Article;} // view
namespace model {class Article;} // model

namespace controler {

class Article {
private:
  model::Sqlite sqlite;
  
public:
  Article(const std::string &path);
  std::vector<model::Article> getArticles(int lastN = 10);
};

} // controler

} // blog

#endif /* BLOG_CONTROLER */
