#ifndef BLOG_MODEL_ARTICLE_H_
#define BLOG_MODEL_ARTICLE_H_

#include <string>
#include "Date.hpp"
#include "Author.hpp"

namespace blog {

namespace model {

class Article {
private:
  std::string title;
  Author author;
  std::string core;
  Date creationDate;
  Date lastUpdateDate;
  
public:
  Article(const std::string &title,
          const std::string &core);

  void setTitle         (const std::string &_title){title = _title;}
  void setAuthor        (const Author &_author){author = _author;}
  void setCore          (const std::string &_core){core = _core;}
  void setCreationDate  (const Date &_date){creationDate = _date;}
  void setLastUpdateDate(const Date &_date){lastUpdateDate = _date;}

  
  const std::string& getTitle()           const {return title;}
  const Author&      getAuthor()          const {return author;}
  const std::string& getCore()            const {return core;}
  const Date&        getCreationDate()    const {return creationDate;}
  const Date&        getLastUpdateDate()  const {return lastUpdateDate;}
};

} // model

} // blog

#endif /* BLOG_MODEL */










