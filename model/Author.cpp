#include "Author.hpp"

namespace blog {

namespace model {

Author::Author(const std::string &login):login(login){}

const std::string &Author::getLogin() const {
  return login;
}

} // model

} // blog
