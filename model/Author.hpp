#ifndef BLOG_MODEL_AUTHOR_H_
#define BLOG_MODEL_AUTHOR_H_

#include <string>

namespace blog {

namespace model {

class Author {
private:
  std::string login;
  
public:
  Author(const std::string &login = "John Doe");
  const std::string &getLogin() const;
};

} // model

} // blog

#endif /* BLOG_MODEL */
