add_library(
  model
  Article.hpp
  Article.cpp
  Author.hpp
  Author.cpp
  Date.cpp
  Date.hpp
  Sqlite.cpp
  Sqlite.hpp
  )

target_link_libraries(model -lsqlite3)
