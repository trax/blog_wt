#include <ctime>
#include "Date.hpp"

namespace blog {

namespace model {

Date::Date(){
  std::time(&time);
}

Date::Date (const std::time_t &time):time (time){}


Date::operator const std::string() const {
   char mbstr[100];
   if (std::strftime(mbstr, 100, "%A %c", std::localtime(&time))) {
     return std::string (mbstr);
   } else {
     return "";
   }
}

} // model

} // blog
