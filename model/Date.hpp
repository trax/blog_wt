#ifndef BLOG_MODEL_DATE_H_
#define BLOG_MODEL_DATE_H_

#include <ctime>
#include <string>


namespace blog {

namespace model {

class Date {
private:
  std::time_t time;
  
public:
  Date();
  Date (const std::time_t &time);
  operator const std::string() const;
};

} // model

} // blog

#endif /* BLOG_MODEL */
