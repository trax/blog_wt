#include <string>
#include <stdexcept>
#include "Sqlite.hpp"

namespace blog {

namespace model {

Sqlite::Sqlite(const std::string &path){

  if(sqlite3_open(path.c_str(), &db)){
    throw std::runtime_error("Can't open database: "); //  + sqlite3_errmsg(db)
  }
}

bool Sqlite::exec(const std::string &request, CallBack *callBack, void *opaq) {
  char *zErrMsg = 0;
  int rc = sqlite3_exec(db, request.c_str(), callBack, opaq, &zErrMsg);
  if(rc != SQLITE_OK){
    fprintf(stderr, "SQL error: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}

// 04  static int callback(void *NotUsed, int argc, char **argv, char **azColName){
// 05    int i;
// 06    for(i=0; i<argc; i++){
// 07      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
// 08    }
// 09    printf("\n");
// 10    return 0;
// 11  }
           
Sqlite::~Sqlite(){
  if(db != nullptr){
    sqlite3_close(db);
  }
}

sqlite3* Sqlite::db = nullptr;

} // model

} // blog
