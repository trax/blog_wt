#ifndef BLOG_MODEL_SQLITE_H_
#define BLOG_MODEL_SQLITE_H_

#include <sqlite3.h>

namespace blog {

namespace model {

class Sqlite {
private:
  static sqlite3 *db;

public:
  typedef int CallBack(void*,int,char**,char**);

  Sqlite(const std::string &path );
  bool exec(const std::string &request, CallBack *,void *opaq);
  ~Sqlite();
};

} // model

} // blog

#endif /* BLOG_MODEL */



