#include "../model/Article.hpp"
#include "Article.hpp"

#include <Wt/WTemplate>
#include <Wt/WString>
#include <Wt/WText>
namespace blog {

namespace view {



Article::Article(model::Article &article, Wt::WContainerWidget *parent) :
  Wt::WContainerWidget(parent),
  article(article){

  addWidget(new Wt::WText(Wt::WString::tr("article-template")
                          .arg(article.getTitle())
                          .arg(article.getAuthor().getLogin())
                          .arg(article.getCreationDate())
                          .arg(article.getCore())));
                          
  
}


} // view

} // blog
