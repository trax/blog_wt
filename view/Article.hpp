#ifndef BLOG_VIEW_ARTICLE_H_
#define BLOG_VIEW_ARTICLE_H_

#include <Wt/WContainerWidget>

namespace blog {

namespace model {
class Article;
} // model

namespace view {



class Article : public Wt::WContainerWidget {
private:
  model::Article &article;
public:
  Article(model::Article &article, Wt::WContainerWidget *parent = 0);
  
};

} // view

} // blog

#endif /* BLOG_VIEW */










