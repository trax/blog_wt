#include "ResourceMessage.hpp"

namespace blog {

namespace view {

ResourceMessage ResourceMessage::instance = ResourceMessage();

ResourceMessage::ResourceMessage() {
  resources.use("bundles/Article", true);
}

} // view

} // blog
