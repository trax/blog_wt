#ifndef BLOG_VIEW_RESSOURCEMESSAGE_H_
#define BLOG_VIEW_RESSOURCEMESSAGE_H_

#include <Wt/WMessageResourceBundle>

namespace blog {
namespace view {

class ResourceMessage {
private:
  static ResourceMessage instance;
  ResourceMessage();
  ResourceMessage& operator= (const ResourceMessage);
  ResourceMessage (const ResourceMessage&){}

  Wt::WMessageResourceBundle resources;
  
public:
  static ResourceMessage& getInstance();
  
};

} // view

} // blog

#endif /* BLOG_VIEW */
