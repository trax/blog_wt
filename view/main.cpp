#include <iostream>
#include <string>

#include <pthread.h>
#include <unistd.h>

#include <Wt/WServer>
#include <Wt/WApplication>
#include <Wt/WLayout>
#include <Wt/WServer>
#include <Wt/WBootstrapTheme>
#include <Wt/WNavigationBar>
#include <Wt/WMenu>
#include <Wt/WStackedWidget>
#include <Wt/WLineEdit>
#include <Wt/WVBoxLayout>

#include <Wt/WText>

#include <boost/bind.hpp>

#include "../controler/Article.hpp"
#include "../view/Article.hpp"
#include "../model/Article.hpp"

namespace blog {

namespace view {



class WebGUI : public Wt::WApplication  {
public:
  WebGUI (const Wt::WEnvironment& env, Wt::WServer &server);
  ~WebGUI(){std::cout << "life is cruel" << std::endl;}

private:
  Wt::WServer &server;
  Wt::WStackedWidget *menuStack;  

  Wt::WVBoxLayout *globalLayout;
  controler::Article article;
  

  Wt::WNavigationBar * createNavigationBar(Wt::WStackedWidget *stack,
                                           Wt::WContainerWidget *parent = 0);
  

};




Wt::WNavigationBar * WebGUI::createNavigationBar(Wt::WStackedWidget *stack, Wt::WContainerWidget *parent) {
  Wt::WNavigationBar * navigation = new Wt::WNavigationBar(parent);
  navigation->setTitle(Wt::WString::tr("STO"));
  navigation->setResponsive(true);

  Wt::WMenu *leftMenu = new Wt::WMenu(stack, parent);
  Wt::WContainerWidget *articles = new Wt::WContainerWidget();
  Wt::WVBoxLayout *articlesLayout = new Wt::WVBoxLayout();

  articles->setLayout(articlesLayout);
  articles->setStyleClass("blog-post");
  
  for (auto it : article.getArticles()){
    articlesLayout->addWidget(new view::Article(it));
    std::cout << "Article> " << it.getTitle() << std::endl;
  }
  
  leftMenu->addItem("Home", articles);

  Wt::WApplication::instance()->enableUpdates(true);
  
  
  std::cout << "Session ID " << WApplication::instance()->sessionId() << std::endl;

  navigation->addMenu(leftMenu);

  Wt::WLineEdit *edit = new Wt::WLineEdit();
  navigation->addSearch(edit, Wt::AlignRight);

  return navigation;
}

WebGUI::WebGUI(const Wt::WEnvironment& env, Wt::WServer &server)
  : Wt::WApplication(env),
    server (server),
    article("../../root/db.db"){

  setTitle(Wt::WString::tr("blog-title"));
  std::cout << __FILE__ << ":" << __FUNCTION__ << ":" << __LINE__ << "> good morning sunshine"  << std::endl;

  useStyleSheet("web/css.css");
  globalLayout = new Wt::WVBoxLayout();
  root()->setLayout(globalLayout);

  setTheme(new Wt::WBootstrapTheme());


  Wt::WApplication::instance()->messageResourceBundle().use("../../root/bundles/article", true);

  root()->setStyleClass("container");
  
  menuStack  = new Wt::WStackedWidget(root()); 
  auto navBar     = createNavigationBar(menuStack, root());
  globalLayout->addWidget(navBar,0);
  globalLayout->addWidget(menuStack,0);

  globalLayout->addStretch(1);
}


} // view

} // blog

Wt::WApplication *createApplication(const Wt::WEnvironment& env, Wt::WServer &server) {
  return new blog::view::WebGUI(env, server);
}

int main(int argc, char **argv) {

  Wt::WServer server(argv[0]);
  
  server.setServerConfiguration(argc, argv, WTHTTP_CONFIGURATION);

  server.addEntryPoint(Wt::Application,
                       boost::bind(createApplication, _1, std::ref(server)));

  if (server.start()) {
    int sig = Wt::WServer::waitForShutdown();
    std::cerr << "Shutting down: (signal = " << sig << ")" << std::endl;
    server.stop();
  }

  return 0;
}

